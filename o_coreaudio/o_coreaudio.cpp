//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//
#include "blc_program.h"
#include "blc_channel.h"

#include <AudioToolbox/AudioToolbox.h>
#include <stdlib.h>
#include <sys/signal.h>

#define DEFAULT_OUTPUT_NAME ":sound<pid>"
#define AUDIO_BUFFERS_NB 2  //Buffers nb to not make sound interuption 2 seems minumum
blc_channel input;
char const *display;

int columns_nb=32, rows_nb=16;

struct timeval timer;

int listenner=1;

/** This is only for display ***/
void resize_both_callback(int new_columns_nb, int new_rows_nb, void *){
    columns_nb=new_columns_nb;
    rows_nb=new_rows_nb-1;
}

void resize_width_callback(int new_columns_nb, int, void *){
    columns_nb=new_columns_nb;
}

void resize_height_callback(int, int new_rows_nb, void *){
    rows_nb=new_rows_nb-1;
}

void display_graph(FILE *file, blc_array *array){
    switch (input.type){
        case 'FL32':
            blc_fprint_float_graph(stderr, input.floats, input.total_length, "sound", columns_nb, rows_nb , 1.0f, -1.0f, "time", "intensity");
            break;
            
        case 'INT8':
            blc_fprint_char_graph(stderr, input.chars, input.total_length, "sound", columns_nb, rows_nb , 128, -127., "time", "intensity");
            break;
    }
    blc_eprint_cursor_up(rows_nb);
}

void init_terminal_display(const char* display){
    
    if (strcmp(display, "F")==0) {
        columns_nb=-1;
        rows_nb=-1;
    }
    else SSCANF(2, display, "%dx%d", &columns_nb, &rows_nb);
    
    if (blc_stderr_ansi) {
        if (rows_nb==-1  && columns_nb==-1) blc_terminal_set_resize_callback(resize_both_callback, NULL);
        else if (columns_nb==-1) blc_terminal_set_resize_callback(resize_width_callback, NULL);
        else if (rows_nb==-1) blc_terminal_set_resize_callback(resize_height_callback, NULL);
        
        //Send a terminal resize signal to initiate sizes
        if (rows_nb==-1  || columns_nb==-1) raise(SIGWINCH);
    }
}
/*** end of display function ***/


void output_queue_callback( void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer ){
    
    
    /*We cannot afford to wait for data */
    if (sem_trywait(input.sem_new_data)==-1){
        //Data is not ready we pause
        fprintf(stderr, "%s: Missing sound sample iteration: %d\n", blc_program_name, blc_loop_iteration);

        if (errno==EAGAIN) AudioQueuePause(inAQ);
        else EXIT_ON_SYSTEM_ERROR("Waiting new data '%s'", input.name);
        
        SYSTEM_ERROR_CHECK(sem_wait(input.sem_new_data), -1, NULL);
        AudioQueueStart(inAQ, NULL);
    }
    
    //Read keyboard
    if (blc_command_loop_start()==BLC_QUIT) AudioQueueStop(inAQ, true);
    else {
        memcpy(inBuffer->mAudioData, input.data, input.size);
        if (input.sem_ack_data) SYSTEM_ERROR_CHECK(sem_post(input.sem_ack_data), -1, NULL);

        //Request for next sound buffer
        inBuffer->mAudioDataByteSize = input.size;
        AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
        
        blc_command_loop_end();
        
        if (display) display_graph(stderr, &input);
    }
}

void start_acquisition(){
    AudioQueueRef queue;
    AudioQueueBufferRef buffer[AUDIO_BUFFERS_NB]; //Like in apple documentation, we use 3 buffers
    AudioStreamBasicDescription audio_format;
    OSStatus err;
    int i;
    
    //OS X audio management
    CLEAR(audio_format);
    audio_format.mSampleRate = 44100;
    audio_format.mFormatID= kAudioFormatLinearPCM;
    
    switch (input.type){
        case 'INT8':
            audio_format.mFormatFlags=kAudioFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
            audio_format.mBitsPerChannel=8;
            break;
        case 'FL32':
            audio_format.mFormatFlags=kAudioFormatFlagIsFloat | kLinearPCMFormatFlagIsPacked;
            audio_format.mBitsPerChannel=8*sizeof(float);
            break;
        default :EXIT_ON_CHANNEL_ERROR(&input, "Only type 'INT8' or 'FL32' is managed.");
            break;
    }
    
    audio_format.mChannelsPerFrame=1;
    audio_format.mBytesPerFrame    = audio_format.mChannelsPerFrame*audio_format.mBitsPerChannel/8;
    audio_format.mFramesPerPacket  = 1; //1 is for ncompressed value
    audio_format.mBytesPerPacket   = audio_format.mBytesPerFrame*audio_format.mFramesPerPacket;
    audio_format.mReserved = 0;
    
    err=AudioQueueNewOutput(&audio_format, output_queue_callback,  NULL,  NULL, NULL, 0, &queue);
    
    FOR(i, AUDIO_BUFFERS_NB) {
        
        err = AudioQueueAllocateBuffer(queue, input.size, &buffer[i]);
        if (err) EXIT_ON_ERROR("Fail to create output audioqueue. Code %d .", err);
        buffer[i]->mAudioDataByteSize= input.size;
        output_queue_callback(NULL, queue, buffer[i]);
    }
    
    
    err=AudioQueueStart(queue, NULL);
    if (err) EXIT_ON_ERROR("Fail to create input audioqueuestart. Code %d .", err);
}

int main(int argc, char **argv)
{
    char const  *type_str;
    const char *input_name;
    uint32_t type='FL32';
    
    blc_program_set_description("Acquire the audio buffer.");
    //  blc_program_add_option(&synchro_new_data, '/', "new_data_post", NULL, "send new data post to all outputs blc_channels", NULL);
    blc_program_add_option(&display, 'd', "display", "F|WIDTHxHEIGHT", "display a text graph. 'F' for fullscreen. -1 for width or heigt for max dimention.", NULL);
    blc_program_add_option(&type_str, 't', "type", "INT8|FL32", "type of the data", "FL32");
    blc_program_add_parameter(&input_name, "blc_channel", 1, "channel to play sound", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    
    type=STRING_TO_UINT32(type_str);
    if (display) init_terminal_display(display);
    
    if (strcmp(DEFAULT_OUTPUT_NAME, input_name)==0) asprintf((char**)&input_name, ":sound%d", getpid());
    
    input.open(input_name, BLC_CHANNEL_READ);
    
    if (input.sem_new_data==NULL) EXIT_ON_SYSTEM_ERROR("This process bas to be synchronized on new incoming data but you blc_channel does not start with ':' or '.'");
    
    blc_command_loop_init(0);
    
    //start_acquisition starts a non blocking thread calling regularly (each time new sound buffer avalable) input_queue_callback.
    start_acquisition();
    
    //We wait for quiting request
    blc_loop_wait_stop();
    
    if (display) blc_eprint_cursor_down(rows_nb);
    
    return EXIT_SUCCESS;
}
