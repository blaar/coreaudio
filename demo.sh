#/usr/bin/bash

cd `dirname $0`/..

#Get the sound and display it and send the result to fftw
./run.sh i_coreaudio | tee >(  ./run.sh f_gnuplot/  --min=-1 --max=1 ) \
    | ./run.sh sig_proc/f_fftw --spectrum | ./run.sh f_gnuplot/  --min=-1 --max=1