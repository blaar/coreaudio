//
//  Created by Arnaud Blanchard on 06/11/14.
//  Copyright ETIS 2014. All rights reserved.
//
#include "blc_program.h"
#include "blc_channel.h"

#include <AudioToolbox/AudioToolbox.h>
#include <stdlib.h>
#include <sys/signal.h>

#define DEFAULT_OUTPUT_NAME ":sound<pid>"

blc_channel output;
char const *display;

int columns_nb=32, rows_nb=16;
size_t buffer_length;

struct timeval timer;

int listenner=1;


#define QUEUE_BUFFERS_NB 1

/** This is only for display ***/
void resize_both_callback(int new_columns_nb, int new_rows_nb, void *){
    columns_nb=new_columns_nb;
    rows_nb=new_rows_nb-1;
}

void resize_width_callback(int new_columns_nb, int, void *){
    columns_nb=new_columns_nb;
}

void resize_height_callback(int, int new_rows_nb, void *){
    rows_nb=new_rows_nb-1;
}

void display_graph(FILE *file, blc_array *array){
    switch (array->type){
        case 'FL32':
            blc_fprint_float_graph(stderr, array->floats, array->total_length, "sound", columns_nb, rows_nb , 1.0f, -1.0f, "time", "intensity");
            break;
            
        case 'INT8':
            blc_fprint_char_graph(stderr, array->chars, array->total_length, "sound", columns_nb, rows_nb , 128, -127., "time", "intensity");
            break;
    }
    blc_eprint_cursor_up(rows_nb);
}

void init_terminal_display(const char* display){
    
    if (strcmp(display, "F")==0) {
        columns_nb=-1;
        rows_nb=-1;
    }
    else SSCANF(2, display, "%dx%d", &columns_nb, &rows_nb);
    
    if (blc_stderr_ansi) {
        if (rows_nb==-1  && columns_nb==-1) blc_terminal_set_resize_callback(resize_both_callback, NULL);
        else if (columns_nb==-1) blc_terminal_set_resize_callback(resize_width_callback, NULL);
        else if (rows_nb==-1) blc_terminal_set_resize_callback(resize_height_callback, NULL);
        
        //Send a terminal resize signal to initiate sizes
        if (rows_nb==-1  || columns_nb==-1) raise(SIGWINCH);
    }
}
/*** end of display function ***/


void input_queue_callback( void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs ){
    
    int result, ready=1;
    
    //Read keyboard, eventually wait for data_ack
    
    if (output.sem_ack_data){
        if (sem_trywait(output.sem_ack_data)==-1){
            if (errno==EAGAIN) {
                fprintf(stderr, "%s: reader not ready iteration %d\n", blc_program_name, blc_loop_iteration);
                ready=0;
            }
            else EXIT_ON_SYSTEM_ERROR("");
        }
    }
    
    
    if (blc_command_loop_start()==0) return;
    
    if (ready){
    memcpy(output.data, inBuffer->mAudioData,  output.size);
    
    result=output.post_new_data();
    
    if (result==0 && listenner) {
        fprintf(stderr, "%s: Nobody listening from iterarion '%d'\n", blc_program_name, blc_loop_iteration);  //Signal new memory
        listenner=0;
    } else if (result==1 && !listenner ){
        fprintf(stderr, "%s:  Someone is listening from iteration '%d'\n", blc_program_name, blc_loop_iteration);
        listenner=1;
    } //Otherwise we are asynchrone
        if (display) display_graph(stderr, &output);

    }

    //Request for next sound buffer
    AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL);
    
    
    blc_command_loop_end();
}

void start_acquisition(AudioQueueRef queue, int samplerate){
    AudioQueueBufferRef buffer[QUEUE_BUFFERS_NB];
    AudioStreamBasicDescription audio_format;
    OSStatus err;
    int i;
    
    //OS X audio management
    CLEAR(audio_format);
    audio_format.mSampleRate = samplerate;
    audio_format.mFormatID= kAudioFormatLinearPCM;
     
    switch (output.type){
        case 'INT8':    audio_format.mFormatFlags=kAudioFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
            audio_format.mBitsPerChannel=8;
            break;
        case 'FL32':    audio_format.mFormatFlags=kAudioFormatFlagIsFloat | kLinearPCMFormatFlagIsPacked;
            audio_format.mBitsPerChannel=8*sizeof(float);
            break;
        default :EXIT_ON_CHANNEL_ERROR(&output, "Only type 'INT8' or 'FL32' is managed.");
            break;
    }
    
    audio_format.mChannelsPerFrame=1;
    audio_format.mBytesPerFrame    = audio_format.mChannelsPerFrame*audio_format.mBitsPerChannel/8;
    audio_format.mFramesPerPacket  = 1; //1 is for ncompressed value
    audio_format.mBytesPerPacket   = audio_format.mBytesPerFrame*audio_format.mFramesPerPacket;
    audio_format.mReserved = 0;
    
    err=AudioQueueNewInput(&audio_format, input_queue_callback,  NULL,  NULL, NULL, 0, &queue);
    if (err){
        if (err==kAudio_ParamError) EXIT_ON_ERROR("Parameter error");
        else EXIT_ON_ERROR("Fail to create input audioqueue. Code %d .", err);
    }
    
    FOR(i, QUEUE_BUFFERS_NB){
        err = AudioQueueAllocateBuffer(queue, output.size, &buffer[i]);
        if (err) EXIT_ON_ERROR("Fail to create input audioqueue. Code %d .", err);
        err = AudioQueueEnqueueBuffer(queue, buffer[i], 0, NULL);
        if (err) EXIT_ON_ERROR("AudioQueueEnqueueBuffer Code %d .", err);
    }
    err=AudioQueueStart(queue, NULL);
    if (err) EXIT_ON_ERROR("Fail to create input audioqueuestart. Code %d .", err);
}

int main(int argc, char **argv)
{
    AudioQueueRef queue=NULL;
    char const *length_str, *type_str, *samplerate_str;
    char const *output_name;
    int samplerate;
    uint32_t type='FL32';
    
    blc_program_set_description("Acquire the audio buffer.");
  //  blc_program_add_option(&synchro_new_data, '/', "new_data_post", NULL, "send new data post to all outputs blc_channels", NULL);
    blc_program_add_option(&display, 'd', "display", "F|WIDTHxHEIGHT", "display a text graph. 'F' for fullscreen. -1 for width or heigt for max dimention.", NULL);
    blc_program_add_option(&samplerate_str, 'S', "samplerate", "integer", "frequency of sampling", "44100");
    blc_program_add_option(&output_name, 'o', "output", "string", "name of the channel where the buffer will be put", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&length_str, 's', "size", "integer", "size of the sound buffer", "4096");
    blc_program_add_option(&type_str, 't', "type", "INT8|FL32", "type of the data", "FL32");
    blc_program_init(&argc, &argv, blc_quit);
    
    SSCANF(1, length_str, "%ld", &buffer_length);
    SSCANF(1, samplerate_str, "%d", &samplerate);

    type=STRING_TO_UINT32(type_str);
    if (display) init_terminal_display(display);
    
    if (strcmp(DEFAULT_OUTPUT_NAME, output_name)==0) {
        asprintf((char**)&output_name, ":sound%d", getpid());
    }
    
    output.create_or_open(output_name, BLC_CHANNEL_WRITE, type, 'LPCM', 1, buffer_length);
    output.publish();
    
    blc_command_loop_init(0);
    //start_acquisition starts a non blocking thread calling regularly (each time new sound buffer avalable) input_queue_callback.
    start_acquisition(queue, samplerate);
    
   //We wait for quiting request
    blc_loop_wait_stop();
    
    AudioQueueStop (queue, true);
    AudioQueueDispose ( queue, true);
    
    
    if (display) blc_eprint_cursor_down(rows_nb);
    
    return EXIT_SUCCESS;
}
